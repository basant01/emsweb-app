import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import {RouterModule} from '@angular/router';
import { CompOneComponent } from './comp-one/comp-one.component';
import { CompTwoComponent } from './comp-two/comp-two.component';
import {HttpClientModule} from '@angular/common/http';
import { SubmitLeaveComponent } from './submit-leave/submit-leave.component';
import {FormsModule} from '@angular/forms';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    CompOneComponent,
    CompTwoComponent,
    SubmitLeaveComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path:'',
        component:CompOneComponent
      },
      {
        path:'one',
        component: CompOneComponent
      },
      {
        path:'two',
        component: CompTwoComponent
      },
      {
        path:'**',
        component: NotFoundComponent
      }
    ]),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
